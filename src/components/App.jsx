import React, { Component, Fragment } from 'react';
import Menu from './Menu';
import List from './List';

export default class App extends Component {
  state = {
    list: {},
    menu: false,
    nextId: 1,
  }

  onToggleMenu = () => {
    const { menu } = this.state;
    this.setState({ menu: !menu });
  }

  onAdd = () => {
    const { list, nextId } = this.state;
    const newList = { ...list, [nextId]: { id: nextId, text: '', show: true } };
    this.setState({ list: newList, nextId: nextId + 1 });
  }

  onEdit = id => (e) => {
    const { list } = this.state;
    const { value } = e.target;
    const newList = { ...list, [id]: { ...list[id], text: value } };

    this.setState({ list: newList });
  }

  onToggleItem = id => () => {
    const { list } = this.state;
    const { show } = list[id];
    const newList = { ...list, [id]: { ...list[id], show: !show } };

    this.setState({ list: newList });
  }

  render() {
    const { menu, list } = this.state;
    return (
      <Fragment>
        <List list={list} />
        <button className='toggle' onClick={this.onToggleMenu}>{`${menu ? 'Hide' : 'Show'} menu`}</button>
        <Menu menu={menu} list={list} onAdd={this.onAdd} onEdit={this.onEdit} onToggleItem={this.onToggleItem} />
      </Fragment>
    );
  }
}
