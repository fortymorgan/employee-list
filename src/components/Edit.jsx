import React from 'react';

const Edit = (props) => {
  const { item, onEdit, onToggleItem } = props;

  return (
    <div className="edit-item">
      <div>{`Item #${item.id}`}</div>
      <textarea cols="40" rows="7" value={item.text} className="edit-field" onInput={onEdit(item.id)} />
      <button className="toggle-item" onClick={onToggleItem(item.id)}>{item.show ? 'Remove' : 'Restore'}</button>
    </div>
  );
};

export default Edit;
