import React from 'react';

const List = (props) => {
  const { list } = props;

  const listArr = Object.values(list);
  const listToShow = listArr.filter(i => i.show);

  return (
    <div className="list">
      {listToShow.map(i => <div className="list-item" key={i.id}>{i.text}</div>)}
    </div>
  );
};

export default List;
