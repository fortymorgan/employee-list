import React from 'react';
import cn from 'classnames';
import Edit from './Edit';

const Menu = (props) => {
  const { list, menu, onAdd, onEdit, onToggleItem } = props;

  const listArr = Object.values(list);

  const menuClass = cn({
    menu: true,
    show: menu,
  });

  return (
    <div className={menuClass}>
      {listArr.map(i => <Edit key={i.id} item={i} onEdit={onEdit} onToggleItem={onToggleItem} />)}
      <button className="add" onClick={onAdd}>Add item</button>
    </div>
  )
};

export default Menu;
